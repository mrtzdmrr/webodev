﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Models;
namespace WebProje.Controllers
{
    public class HomeController : Controller
    {
                ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            HttpContext.Session["csayfa"] = menusecenekler.anasayfa;
            AnaSayfaModeller model = new AnaSayfaModeller { _ilan = db.IlanVers.OrderByDescending(x=>x.Tarih).Take(4).ToList() , _yorum=db.Yorums.OrderByDescending(x=>x.Tarih).Take(3).ToList() };
            return View(model);

        }


        [Authorize]
        public ActionResult YurtBul()
        {
            HttpContext.Session["csayfa"] = menusecenekler.yurtlar;
            AnaSayfaModeller model = new AnaSayfaModeller { _ilan = db.IlanVers.OrderByDescending(x => x.Tarih).ToList() };
            return View(model);
        }
        [Authorize]
        
        public ActionResult ApartBul()
        {
            HttpContext.Session["csayfa"] = menusecenekler.apartlar;

            AnaSayfaModeller model = new AnaSayfaModeller { _ilan = db.IlanVers.OrderByDescending(x => x.Tarih).ToList() };
            return View(model);
        }
       
        
      

        public ActionResult Contact()
        {
            HttpContext.Session["csayfa"] = menusecenekler.iletisim;

            return View();
        }
        [Authorize]
        
        public ActionResult _ilanParca()
        {
            AnaSayfaModeller model = new AnaSayfaModeller { _ilan= db.IlanVers.ToList() };
            return View(model);
        }


    }
}