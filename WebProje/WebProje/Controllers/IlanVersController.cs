﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebProje.Models;

namespace WebProje.Controllers
{
    public class IlanVersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: IlanVers
        [Authorize(Roles = "admin")]

        public ActionResult Index(string siralama , string arama )
        {
            HttpContext.Session["csayfa"] = menusecenekler.yurtlar;

            ViewBag.NameSortParm = string.IsNullOrEmpty(siralama) ? "KurumAdi_azalan" : "";
            ViewBag.DateSortParm = siralama == "tarih_artan" ? "tarih_azalan" : "tarih_artan";





            var ilanlar = from s in db.IlanVers
                          select s;


            //arama---
            if(!String.IsNullOrEmpty(arama))
            {
                ilanlar = ilanlar.Where(s => s.KurumAdi.ToUpper().Contains(arama.ToUpper())
                      || s.KisiAdi.ToUpper().Contains(arama.ToUpper()));
            }
            //--------

            //siralama---
            switch(siralama)
            {
                case  "KurumAdi_azalan":
                    ilanlar = ilanlar.OrderByDescending(s => s.KurumAdi);
                    break;
                case "tarih_artan":
                    ilanlar = ilanlar.OrderBy(s => s.Tarih);
                    break;
                case "tarih_azalan":
                    ilanlar = ilanlar.OrderByDescending(s => s.Tarih);
                    break;
                default:
                    ilanlar = ilanlar.OrderBy(s => s.KurumAdi);
                    break;

            }
            //------------



            return View(ilanlar.ToList());
        }

        // GET: IlanVers/Details/5
        [Authorize]

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IlanVer ilanVer = db.IlanVers.Find(id);
            if (ilanVer == null)
            {
                return HttpNotFound();
            }
            return View(ilanVer);
        }
        [Authorize(Roles = "admin,sahip")]

        // GET: IlanVers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IlanVers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "admin,sahip")]

        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,IlanTipi,Adres,Telefon,WebSitesi,KurumAdi,KisiAdi,Fotograf1,Fotograf2,Fotograf3,Aciklamalar")]  IlanModel ilanVer, HttpPostedFileBase Foto1, HttpPostedFileBase Foto2, HttpPostedFileBase Foto3)
        {
            if (ModelState.IsValid)
            {
                ilanVer.Tarih = System.DateTime.Now;
                string DosyaAdi = Guid.NewGuid().ToString().Replace('-', '1');// sunucumuzda bulunmayan rasgele bir string atıyor aralardaki - leri herhangi bir karakterle değiştiriyoruz
                string uzanti = System.IO.Path.GetExtension(Foto1.FileName);
                string veri_yolu = "~/Content/images/" + DosyaAdi + uzanti;
                Foto1.SaveAs(Server.MapPath(veri_yolu));
                string URL1 = "/Content/images/" + DosyaAdi + uzanti;// başında ~ yok bunu ekleyeceksin veritabanına

                DosyaAdi = Guid.NewGuid().ToString().Replace('-', '1');// sunucumuzda bulunmayan rasgele bir string atıyor aralardaki - leri herhangi bir karakterle değiştiriyoruz
                uzanti = System.IO.Path.GetExtension(Foto2.FileName);
                veri_yolu = "~/Content/images/" + DosyaAdi + uzanti;
                Foto2.SaveAs(Server.MapPath(veri_yolu));
                string URL2 = "/Content/images/" + DosyaAdi + uzanti;// başında ~ yok bunu ekleyeceksin veritabanına

                DosyaAdi = Guid.NewGuid().ToString().Replace('-', '1');// sunucumuzda bulunmayan rasgele bir string atıyor aralardaki - leri herhangi bir karakterle değiştiriyoruz
                uzanti = System.IO.Path.GetExtension(Foto3.FileName);
               veri_yolu = "~/Content/images/" + DosyaAdi + uzanti;
                Foto3.SaveAs(Server.MapPath(veri_yolu));
                string URL3 = "/Content/images/" + DosyaAdi + uzanti;// başında ~ yok bunu ekleyeceksin veritabanına

                string id = User.Identity.GetUserId();
                var kullanici = db.Users.Where(x => x.Id == id).SingleOrDefault();
                db.Users.Where(x => x.Id == id).SingleOrDefault();

                var user = new IlanVer { ID = ilanVer.ID, IlanTipi = ilanVer.IlanTipi, Adres = ilanVer.Adres, Tarih = ilanVer.Tarih, Telefon = ilanVer.Telefon, WebSitesi = ilanVer.WebSitesi, KurumAdi = ilanVer.KurumAdi, KisiAdi = ilanVer.KisiAdi, Fotograf1 = URL1, Fotograf2 = URL2, Fotograf3 = URL3, Aciklamalar = ilanVer.Aciklamalar, Kullanicilar = db.Users.Where(x => x.Id == id).SingleOrDefault() };



                db.IlanVers.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            return View(ilanVer);
        }


































        // GET: IlanVers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IlanVer ilanVer = db.IlanVers.Find(id);
            if (ilanVer == null)
            {
                return HttpNotFound();
            }
            return View(ilanVer);
        }

        // POST: IlanVers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,IlanTipi,Adres,Tarih,Telefon,WebSitesi,KurumAdi,KisiAdi,Fotograf1,Fotograf2,Fotograf3,Aciklamalar")] IlanVer ilanVer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ilanVer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ilanVer);
        }

        // GET: IlanVers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IlanVer ilanVer = db.IlanVers.Find(id);
            if (ilanVer == null)
            {
                return HttpNotFound();
            }
            return View(ilanVer);
        }

        // POST: IlanVers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IlanVer ilanVer = db.IlanVers.Find(id);
            db.IlanVers.Remove(ilanVer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
