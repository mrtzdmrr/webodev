﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebProje.Models;

namespace WebProje.Controllers
{
    public class YorumsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [Authorize(Roles = "admin")]

        // GET: Yorums
        public ActionResult Index(string siralama, string arama)
        {
            HttpContext.Session["csayfa"] = menusecenekler.yorumlar;

            // burada session atamadığın için anasayfa olarak algılıyor artık hmm anladım sağolasın kanka ne demek 
            ViewBag.DateSortParm = siralama == "tarih_artan" ? "tarih_azalan" : "tarih_artan";

            var yorumlar = from s in db.Yorums
                             select s;
            if (!String.IsNullOrEmpty(arama))
            {
                yorumlar = yorumlar.Where(s => s.Yorum1.ToUpper().Contains(arama.ToUpper()));
            }
            switch (siralama)
            {

                case "tarih_artan":
                    yorumlar = yorumlar.OrderBy(s => s.Tarih);
                    break;
                case "tarih_azalan":
                    yorumlar = yorumlar.OrderByDescending(s => s.Tarih);
                    break;


            }




            return View(yorumlar.ToList());
        }

        // GET: Yorums/Details/5
        [Authorize]

        public ActionResult Details(int? id)
        {
            HttpContext.Session["csayfa"] = menusecenekler.yorumlar;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yorum yorum = db.Yorums.Find(id);
            if (yorum == null)
            {
                return HttpNotFound();
            }
            return View(yorum);
        }

        // GET: Yorums/Create
        [Authorize]

        public ActionResult Create()
        {
            HttpContext.Session["csayfa"] = menusecenekler.yorumlar;

            return View();
        }

        // POST: Yorums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]

        public ActionResult Create([Bind(Include = "ID,Yorum1")] Yorum yorum)
        {
            HttpContext.Session["csayfa"] = menusecenekler.yorumlar;

            if (ModelState.IsValid)
            {
                yorum.Tarih = System.DateTime.Now;
                string id = User.Identity.GetUserId();
                var kullanici = db.Users.Where(x => x.Id == id).SingleOrDefault();
                yorum.Kullanicilar = db.Users.Where(x => x.Id == id).SingleOrDefault();

                db.Yorums.Add(yorum);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            return View(yorum);
        }

        // GET: Yorums/Edit/5
        [Authorize]

        public ActionResult Edit(int? id)
        {
            HttpContext.Session["csayfa"] = menusecenekler.yorumlar;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yorum yorum = db.Yorums.Find(id);
            if (yorum == null)
            {
                return HttpNotFound();
            }
            return View(yorum);
        }

        // POST: Yorums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Yorum1,Tarih")] Yorum yorum)
        {
            HttpContext.Session["csayfa"] = menusecenekler.yorumlar;

            if (ModelState.IsValid)
            {
                db.Entry(yorum).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(yorum);
        }
        [Authorize]

        // GET: Yorums/Delete/5
        public ActionResult Delete(int? id)
        {
            HttpContext.Session["csayfa"] = menusecenekler.yorumlar;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yorum yorum = db.Yorums.Find(id);
            if (yorum == null)
            {
                return HttpNotFound();
            }
            return View(yorum);
        }

        // POST: Yorums/Delete/5
        [Authorize]

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HttpContext.Session["csayfa"] = menusecenekler.yorumlar;

            Yorum yorum = db.Yorums.Find(id);
            db.Yorums.Remove(yorum);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
