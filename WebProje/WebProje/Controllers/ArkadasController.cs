﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebProje.Models;

namespace WebProje.Controllers
{
    public class ArkadasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Arkadas
        [Authorize(Roles = "admin")]
        public ActionResult Index(string siralama, string arama)
        {
            HttpContext.Session["csayfa"] = menusecenekler.arkadas;
            ViewBag.DateSortParm = siralama == "tarih_artan" ? "tarih_azalan" : "tarih_artan";

            var arkadaslar = from s in db.Arkadas
                          select s;
            if (!String.IsNullOrEmpty(arama))
            {
               arkadaslar = arkadaslar.Where(s => s.ArkadasDetay.ToUpper().Contains(arama.ToUpper()));
            }
            switch (siralama)
            {
                
                case "tarih_artan":
                    arkadaslar = arkadaslar.OrderBy(s => s.Tarih);
                    break;
                case "tarih_azalan":
                    arkadaslar = arkadaslar.OrderByDescending(s => s.Tarih);
                    break;
              

            }







            return View(arkadaslar.ToList());
        }

        [Authorize(Roles = "admin,ogrenci")]

        // GET: Arkadas/Details/5
        public ActionResult Details(int? id)
        {
            HttpContext.Session["csayfa"] = menusecenekler.arkadas;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Arkadas arkadas = db.Arkadas.Find(id);
            if (arkadas == null)
            {
                return HttpNotFound();
            }
            return View(arkadas);
        }

        // GET: Arkadas/Create
        [Authorize(Roles = "admin,ogrenci")]

        public ActionResult Create()
        {
            HttpContext.Session["csayfa"] = menusecenekler.arkadas;

            return View();
        }

        // POST: Arkadas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin,ogrenci")]

        public ActionResult Create([Bind(Include = "ID,ArkadasDetay")] Arkadas arkadas)
        {
            HttpContext.Session["csayfa"] = menusecenekler.arkadas;

            if (ModelState.IsValid)
            {
                arkadas.Tarih = System.DateTime.Now;

                string id = User.Identity.GetUserId();
                var kullanici = db.Users.Where(x => x.Id == id).SingleOrDefault();
                arkadas.Kullanicilar = db.Users.Where(x => x.Id == id).SingleOrDefault();
                db.Arkadas.Add(arkadas);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            return View(arkadas);
        }

        // GET: Arkadas/Edit/5
        [Authorize(Roles = "admin,ogrenci")]

        public ActionResult Edit(int? id)
        {
            HttpContext.Session["csayfa"] = menusecenekler.arkadas;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Arkadas arkadas = db.Arkadas.Find(id);
            if (arkadas == null)
            {
                return HttpNotFound();
            }
            return View(arkadas);
        }

        // POST: Arkadas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin,ogrenci")]

        public ActionResult Edit([Bind(Include = "ID,ArkadasDetay,Tarih")] Arkadas arkadas)
        {
            HttpContext.Session["csayfa"] = menusecenekler.arkadas;

            if (ModelState.IsValid)
            {
                db.Entry(arkadas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(arkadas);
        }

        // GET: Arkadas/Delete/5
        [Authorize(Roles = "admin,ogrenci")]

        [Authorize(Roles = "admin,ogrenci")]

        public ActionResult Delete(int? id)
        {
            HttpContext.Session["csayfa"] = menusecenekler.arkadas;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Arkadas arkadas = db.Arkadas.Find(id);
            if (arkadas == null)
            {
                return HttpNotFound();
            }
            return View(arkadas);
        }

        // POST: Arkadas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin,ogrenci")]

        public ActionResult DeleteConfirmed(int id)
        {
            HttpContext.Session["csayfa"] = menusecenekler.arkadas;

            Arkadas arkadas = db.Arkadas.Find(id);
            db.Arkadas.Remove(arkadas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
