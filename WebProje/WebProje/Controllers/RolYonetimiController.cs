﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace WebProje.Controllers
//{
//    public class RolYonetimiController : Controller
//    {
//        // GET: RolYonetimi
//        public ActionResult Index()
//        {
//            return View();
//        }
//    }
//}
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Models;

namespace WebProje.Controllers
{
    public class RolYonetimiController : Controller
    {

        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/RolYonetimi
        [Authorize(Roles = "admin")]

        public ActionResult Index()
        {
            var rolestore = new RoleStore<IdentityRole>(context);
            var rolemanager = new RoleManager<IdentityRole>(rolestore);
            var model = rolemanager.Roles.ToList();

            return View(model);
        }
        [Authorize(Roles = "admin")]

        public ActionResult RolEklemek()
        {
            return View();
        }
        [Authorize(Roles = "admin")]

        [HttpPost]
        public ActionResult RolEklemek(RolEkleModel rol)
        {
            var rolestore = new RoleStore<IdentityRole>(context);
            var rolemanager = new RoleManager<IdentityRole>(rolestore);
            if (rolemanager.RoleExists(rol.rolad) == false)
            {
                rolemanager.Create(new IdentityRole(rol.rolad));
            }
            return RedirectToAction("Index","RolYonetimi");
        }
        [Authorize(Roles = "admin")]

        public ActionResult rolkullaniciekle()
        {
            return View();
        }
        [HttpPost]
        [Authorize(Roles = "admin")]

        public ActionResult rolkullaniciekle(RolKullaniciEkleModel model)
        {
            var rolestore = new RoleStore<IdentityRole>(context);
            var rolemanager = new RoleManager<IdentityRole>(rolestore);


            var userstore = new UserStore<ApplicationUser>(context);
            var usermanager = new UserManager<ApplicationUser>(userstore);

            var kullanici = usermanager.FindByName(model.KullaniciAdi);

            if (!usermanager.IsInRole(kullanici.Id, model.RolAdi))
            {
                usermanager.AddToRole(kullanici.Id, model.RolAdi);
            }
            return RedirectToAction("Index", "RolYonetimi");
        }
    }
}