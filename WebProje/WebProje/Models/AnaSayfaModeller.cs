﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProje.Models
{
    public class AnaSayfaModeller
    {
        public IEnumerable<IlanVer> _ilan { get; set; }
        public IEnumerable<Yorum> _yorum { get; set; }

        public IEnumerable<Arkadas> _arkadas { get; set; }

    }
}