﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace WebProje.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ApplicationUser()
        {
            this.Arkadas = new HashSet<Arkadas>();
            this.IlanVer = new HashSet<IlanVer>();
            this.Yorum = new HashSet<Yorum>();
        }

        public string Resim { get; set; }
        public string Telefon { get; set; }
        public string Ad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Arkadas> Arkadas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IlanVer> IlanVer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Yorum> Yorum { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public partial class IlanVer
    {
        public int ID { get; set; }
        public string IlanTipi { get; set; }
        public string Adres { get; set; }
        public System.DateTime Tarih { get; set; }
        public string Telefon { get; set; }
        public string WebSitesi { get; set; }
        public string KurumAdi { get; set; }
        public string KisiAdi { get; set; }
        public string Fotograf1 { get; set; }
        public string Fotograf2 { get; set; }
        public string Fotograf3 { get; set; }
        public string Aciklamalar { get; set; }

        public virtual ApplicationUser Kullanicilar { get; set; }
    }
    public partial class Arkadas
    {
        public int ID { get; set; }
        public string ArkadasDetay { get; set; }
        public System.DateTime Tarih { get; set; }
        public virtual ApplicationUser Kullanicilar { get; set; }
    }
    public partial class Yorum
    {
        public int ID { get; set; }
        public string Yorum1 { get; set; }
        public System.DateTime Tarih { get; set; }
        public virtual ApplicationUser Kullanicilar { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("VeriTabani", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<WebProje.Models.IlanVer> IlanVers { get; set; }

        public System.Data.Entity.DbSet<WebProje.Models.Arkadas> Arkadas { get; set; }

        public System.Data.Entity.DbSet<WebProje.Models.Yorum> Yorums { get; set; }
    }
}