﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProje.Models
{
    public class KullaniciModel
    {
        public string kullaniciID { get; set; }
        public string kullaniciresmi { get; set; }
        public string kullaniciAdi { get; set; }
    }
    public enum menusecenekler
    {
        anasayfa,yurtlar,apartlar,arkadas,yorumlar,iletisim
    }
    public class IlanModel
    {
        public int ID { get; set; }
        public string IlanTipi { get; set; }
        public string Adres { get; set; }
        public System.DateTime Tarih { get; set; }
        public string Telefon { get; set; }
        public string WebSitesi { get; set; }
        public string KurumAdi { get; set; }
        public string KisiAdi { get; set; }// kafa durdu da biraz inceliyom ondan benim de :D
        public HttpPostedFileBase Foto1 { get; set; }
        public HttpPostedFileBase Foto2 { get; set; }
        public HttpPostedFileBase Foto3 { get; set; }
        public string Aciklamalar { get; set; }
    }
}