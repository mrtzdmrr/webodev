// <auto-generated />
namespace WebProje.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class mg_1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(mg_1));
        
        string IMigrationMetadata.Id
        {
            get { return "201612151022452_mg_1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
